# waydroid-openSUSE-setup

## Installation and Setup Guide for Waydroid on openSUSE Tumbleweed and openSUSE Leap.

NOTE: This script is tested on Tumbleweed and based distros, install packages for Leap similarly.

 Tested System Details:
* OS: OpenSUSE Tumbleweed, Gecko Linux (Rolling)
* Kernel: 6.1.10-1
* CPU: AMD Ryzen 5 3500U

Instructions:


### 1. install libgbinder1:

> preferred repo [home:sp1rit](https://download.opensuse.org/repositories/home:sp1rit/openSUSE_Tumbleweed/home:sp1rit.repo)
* install manually using [libgbinder1](https://software.opensuse.org/download/package?package=libgbinder1&project=home%3Asp1rit)
> any other repo should also work

### 2. install python310-gbinder:

> preferred repo [home:bhwachter:desktop](https://download.opensuse.org/repositories/home:bhwachter:desktop/openSUSE_Tumbleweed/home:bhwachter:desktop.repo) :
* install manually using [python310-gbinder](https://software.opensuse.org/download/package?package=python310-gbinder&project=home%3Abhwachter%3Adesktop)
> any other repo should also work

### 3. install anbox-kmp-default and anbox-modules-autoload

> preferred repo [home:simopil](https://download.opensuse.org/repositories/home:simopil/openSUSE_Tumbleweed/home:simopil.repo) :
* install anbox-kmp-default manually using [anbox-kmp-default](https://software.opensuse.org/download/package?package=anbox-kmp-default&project=home%3Asimopil)
* install anbox-modules-autoload manually using same repo as above (maybe its already installed with the above package as dependency)
> other repos might not work!

### 4. install lxc, iptables, dnsmasq :

```
sudo zypper install lxc iptables dnsmasq
```

### 5. install liblxc1 :

> preferred repo [home:dirkmueller:Factory](https://download.opensuse.org/repositories/home:dirkmueller:Factory/standard/home:dirkmueller:Factory.repo)
* install manually using [liblxc1](https://software.opensuse.org/download/package?package=liblxc1&project=home%3Adirkmueller%3AFactory)

### 6. install waydroid :

> preferred repo [home:bhwachter:desktop](https://download.opensuse.org/repositories/home:bhwachter:desktop/openSUSE_Tumbleweed/home:bhwachter:desktop.repo) :
* install manually using [waydroid](https://software.opensuse.org/download/package?package=waydroid&project=home%3Abhwachter%3Adesktop)
> other repos might be out of date!

### At this point, it's recommended a reboot!

### 7. initialize waydroid:
```
sudo waydroid init
```
### 8. enable service (systemd):
```
sudo systemctl enable --now waydroid-container
```
## Now waydroid should work using :
```
sudo waydroid container start
waydroid session start
waydroid show-full-ui
```

## But you will most probably encounter the following issues:

### [gbinder] WARNING: Service manager /dev/binder has died

Solution:

* install grubby (available on openSUSE:Factory + or use [this link](https://software.opensuse.org/download/package?package=grubby&project=Virtualization))

```
sudo grubby --update-kernel="/boot/vmlinuz-$(uname -r)" --args="psi=1"
reboot
```

> for more info see https://github.com/waydroid/waydroid/issues/136

### Waydroid Apparmor conflict

> openSUSE uses apparmor so you might have this issue

Solution :

* add
```
@{run}/waydroid-lxc/ r,
@{run}/waydroid-lxc/* rw,
```
to 
```
/etc/apparmor.d/usr.sbin.dnsmasq
```





### For image related problem -

* Create the system images directory:
```
sudo mkdir -p /usr/share/waydroid-extra/images
```
* Grab the latest system.img and vendor.img

- download archive from [sourceforge](https://sourceforge.net/projects/waydroid/files/images/system/lineage/waydroid_x86_64/)

- extract both system.img and vendor.img to 
```
/usr/share/waydroid-extra/images
```






